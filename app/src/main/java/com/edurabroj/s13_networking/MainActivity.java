package com.edurabroj.s13_networking;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {
    Button btnLoad;
    TextView tvInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLoad = findViewById(R.id.btnLoad);
        tvInfo = findViewById(R.id.tvInfo);

        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetData().execute();
            }
        });
    }

    private class GetData extends AsyncTask<Void,Void,String>{
        public static final String HOST="api.geonames.org";
        public static final String USERNAME="edurabroj";
        public static final String HTTP_GET_URL =
                "GET /earthquakesJSON?north=44.1&south=-9.9&east=-22.4&west=55.2&username="+USERNAME +
                " HTTP/1.1" + "\n" +
                " Host: " + HOST + "\n" +
                " Connection: close\n\n";

        @Override
        protected void onPreExecute() {
            tvInfo.setText("");
            btnLoad.setEnabled(false);
        }

        @Override
        protected String doInBackground(Void... voids) {
            String info="";
            Socket socket = null;
            try {
//                Log.d("Información",info);
                socket = new Socket(HOST,80);
                PrintWriter printWriter = new PrintWriter(socket.getOutputStream(),true);
                printWriter.println(HTTP_GET_URL);

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                while(bufferedReader.ready()){
                    info+=bufferedReader.readLine();
                }
//                String linea;
//                while((linea=bufferedReader.readLine())!=null){
//                    info+=linea;
//                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return info;
        }

        @Override
        protected void onPostExecute(String info) {
            showMeg(info);
            tvInfo.setText(info);
            btnLoad.setEnabled(true);
        }
    }

    void showMeg(String msg){
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }
}
